#Elastic File System
resource "aws_efs_file_system" "wordpress-fs" {
  creation_token = "wordpress-fs"
  tags = {
    Name = "Wordpress FS"
  }
}

# Provides an efs for the wordpress-a
resource "aws_efs_mount_target" "wordpress-a" {
  file_system_id = "${aws_efs_file_system.wordpress-fs.id}"
  subnet_id      = "${aws_subnet.public-sub-a.id}"
  security_groups = ["${aws_security_group.efs.id}"]
}

# Provides an efs for the wordpress-b
resource "aws_efs_mount_target" "wordpress-b" {
  file_system_id = "${aws_efs_file_system.wordpress-fs.id}"
  subnet_id      = "${aws_subnet.public-sub-b.id}"
  security_groups = ["${aws_security_group.efs.id}"]
}

#output for serverconfig.tpl
output "efs-id" {
    value = "${aws_efs_file_system.wordpress-fs.id }"
}
