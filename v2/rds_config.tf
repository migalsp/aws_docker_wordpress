#RDS
#Provides an RDS DB subnet group resource
resource "aws_db_subnet_group" "dbsubnetgroup" {
  name        = "rds-subnet-group"
  description = "group of subnets"
  subnet_ids  = ["${aws_subnet.private-sub-a.id}", "${aws_subnet.private-sub-b.id}"]
  tags = {
    Name = "wordpress-db-subnet"
  }

}

#Provides an RDS instance resource
resource "aws_db_instance" "wordpress-db" {
# depends on aws_security_group.web
  depends_on             = ["aws_security_group.web"]
  identifier             = "wordpress-db"
  allocated_storage      = "5"
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  name                   = "wordpress"
  username               = "dbadmin"
  password               = "wordpress2019"
# multizone feature (standby server in another zone). Not free tier
  multi_az               = true
# no DBSnapshot is created before the DB instance is deleted
  skip_final_snapshot    = true
  vpc_security_group_ids = ["${aws_security_group.db.id}"]
  db_subnet_group_name   = "${aws_db_subnet_group.dbsubnetgroup.id}"
  tags = {
    Name = "wordpress-db"
  }

}

#output for serverconfig.tpl
output "rdshost" {

  value = "${aws_db_instance.wordpress-db.address}"

}
